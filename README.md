# InfluxDB module

InfluxDB is an open-source time series database developed by the company InfluxData. It is used for storage and retrieval of time series data in fields such as operations monitoring, application metrics, Internet of Things sensor data, and real-time analytics.

For the official page of the module, visit the
[project page](https://www.drupal.org/project/influxdb).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/influxdb).

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Usage
- Maintainers

## Requirements

- [Key](https://drupal.org/project/key)
  - For storing the organization token in a secure way


## Recommended modules

- [ECA](https://drupal.org/project/eca)
  - Required when you install the sub-module `influxdb_eca`

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Navigate to Administration > Configuration > Search and metadata > InfluxDB (`/admin/config/services/influxdb`)

## Usage

This module, in its very minimal state, provides a configuration-entity for storing connection-details to your InfluxDB-instance. When those details are provided, the accompanied client-service `influxdb.services.client` can be used to create instances of the [influxdata/influxdb-client-php](https://github.com/influxdata/influxdb-client-php)-package.

```php
/** @var \InfluxDB2\Service\HealthService $service */
$service = Drupal::service('influxdb.services.client')
  ->createService(HealthService::class);
```

### InfluxDB - Bucket
The submodule `influxdb_bucket` allows a user to configure InfluxDB buckets directly from within Drupal. A config-entity is used to represent those external buckets.

Navigate to Administration > Configuration > Search and metadata > InfluxDB > Buckets (`/admin/config/services/influxdb/buckets`)

### InfluxDB - Bucket ECA
The submodule `influxdb_bucket_eca` allows a user to create and send Points to a Bucket by using actions in [ECA](https://drupal.org/module/eca)-models.

## Maintainers

- Jasper Lammens - [@lammensj](https://www.drupal.org/u/lammensj)
