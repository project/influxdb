<?php

namespace Drupal\influxdb_bucket;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Buckets.
 */
class BucketListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['retention_seconds'] = $this->t('Retention period (in seconds)');
    $header['status'] = $this->t('Status');
    $header['default'] = $this->t('Default');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\influxdb_bucket\BucketInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['retention_seconds'] = $entity->getRetentionSeconds();
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    $row['default'] = $entity->isDefault() ? $this->t('Default') : $this->t('Not default');

    return $row + parent::buildRow($entity);
  }

}
