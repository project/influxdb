<?php

namespace Drupal\influxdb_bucket\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\influxdb_bucket\Services\BucketManager\BucketManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Bucket form.
 *
 * @property \Drupal\influxdb_bucket\BucketInterface $entity
 */
class BucketForm extends EntityForm {

  /**
   * The bucket manager.
   *
   * @var \Drupal\influxdb_bucket\Services\BucketManager\BucketManagerInterface
   */
  protected BucketManagerInterface $bucketManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BucketForm {
    $instance = parent::create($container);
    $instance->bucketManager = $container->get('influxdb_bucket.services.bucket_manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\influxdb_bucket\Entity\Bucket::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['retention_seconds'] = [
      '#type' => 'select',
      '#title' => $this->t('Retention period'),
      '#description' => $this->t('In seconds'),
      '#options' => [
        0 => $this->t('Never'),
        3600 => $this->t('1 hour'),
        21600 => $this->t('6 hours'),
        43200 => $this->t('12 hours'),
        86400 => $this->t('24 hours'),
        172800 => $this->t('48 hours'),
        259200 => $this->t('72 hours'),
        604800 => $this->t('7 days'),
        1209600 => $this->t('14 days'),
        2592000 => $this->t('30 days'),
        7776000 => $this->t('90 days'),
        31557600 => $this->t('1 year'),
      ],
      '#default_value' => $this->entity->getRetentionSeconds(),
      '#required' => TRUE,
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Status'),
      '#default_value' => $this->entity->status(),
    ];

    $form['default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Default'),
      '#default_value' => $this->entity->isDefault(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['@label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created Bucket @label', $message_args)
      : $this->t('Updated Bucket @label', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    $response = $this->bucketManager->upsertBucket($form_state->getValues());
    $message = $response
      ? $this->t('Successfully created/updated remote Bucket.')
      : $this->t('Creating/updating remote Bucket failed.');
    $this->messenger()->addMessage($message, $response ? MessengerInterface::TYPE_STATUS : MessengerInterface::TYPE_ERROR);

    return $result;
  }

}
