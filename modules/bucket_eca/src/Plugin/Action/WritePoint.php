<?php

namespace Drupal\influxdb_bucket_eca\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\influxdb_bucket\BucketInterface;
use InfluxDB2\Point;
use InfluxDB2\Service\BucketsService;
use InfluxDB2\Writer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Action to write a point.
 *
 * @Action(
 *   id = "influxdb_write_point",
 *   label = @Translation("Write Point")
 * )
 */
class WritePoint extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected NormalizerInterface $serializer;

  /**
   * The Buckets-service.
   *
   * @var \InfluxDB2\Service\BucketsService
   */
  protected BucketsService $bucketsService;

  /**
   * The Writer-service.
   *
   * @var \InfluxDB2\Writer
   */
  protected Writer $writerApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->serializer = $container->get('serializer');

    $client = $container->get('influxdb.services.client');
    $instance->writerApi = $client->createWriteApi();
    $instance->bucketsService = $client->createService(BucketsService::class);

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE): bool|AccessResultInterface {
    $access = AccessResult::allowed();

    if (!$this->tokenService->hasTokenData($this->configuration['eca_token_name'])) {
      $access = AccessResult::forbidden(sprintf('Token-data for token \'%s\' not set.', $this->configuration['eca_token_name']));
    }

    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $data = $this->tokenService->getTokenData($this->configuration['eca_token_name']);
    $data = (array) $this->serializer->normalize($data);
    $point = Point::fromArray($data);

    /** @var \Drupal\influxdb_bucket\BucketInterface $config */
    $config = $this->entityTypeManager->getStorage('influxdb_bucket')
      ->load($this->configuration['bucket']);
    $buckets = $this->bucketsService->getBuckets(NULL, NULL, 1, NULL, NULL, NULL, $config->label());

    $this->writerApi->write($point, $point->getPrecision(), $buckets->getBuckets()[0]->getId());
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $config = parent::defaultConfiguration();
    $config['eca_token_name'] = '';
    $config['bucket'] = '';

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['eca_token_name'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Point token name'),
      '#default_value' => $this->configuration['eca_token_name'],
      '#description' => $this->t('The name of the token that contains the Point.'),
    ];

    $options = array_reduce($this->entityTypeManager->getStorage('influxdb_bucket')->loadMultiple(), function (array $carry, BucketInterface $bucket) {
      $carry[$bucket->id()] = $bucket->label();

      return $carry;
    }, []);

    $form['bucket'] = [
      '#type' => 'select',
      '#options' => $options,
      '#required' => TRUE,
      '#title' => $this->t('Bucket'),
      '#default_value' => $this->configuration['bucket'],
      '#description' => $this->t('Select the Bucket to write to.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['eca_token_name'] = $form_state->getValue('eca_token_name');
    $this->configuration['bucket'] = $form_state->getValue('bucket');
  }

}
