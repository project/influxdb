<?php

namespace Drupal\influxdb_test;

use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;

/**
 * Middleware to interact with InfluxDB.
 */
class InfluxDBMiddleware {

  /**
   * Invoked method that returns a promise.
   */
  public function __invoke() {
    return function ($handler) {
      return function (RequestInterface $request, array $options) use ($handler) {
        $uri = $request->getUri();

        if ($uri->getHost() === 'influxdb') {
          return $this->createPromise($request);
        }

        return $handler($request, $options);
      };
    };
  }

  /**
   * Callback of the middleware.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The http-request.
   *
   * @return \GuzzleHttp\Promise\FulfilledPromise
   *   Returns a promise containing a response.
   */
  protected function createPromise(RequestInterface $request) {
    $body = json_decode((string) $request->getBody(), TRUE);

    switch (TRUE) {
      case str_contains($body['query'], '(bucket: "example-validresponse")'):
        $data = <<<EOD
        #group,false,false,true,true,false,false,true,true,true,true
        #datatype,string,long,dateTime:RFC3339,dateTime:RFC3339,dateTime:RFC3339,double,string,string,string,string
        #default,mean,,,,,,,,,
        ,result,table,_start,_stop,_time,_value,_field,_measurement,host,region
        ,,0,2022-12-31T05:41:24Z,2023-01-31T05:41:24.001Z,2023-01-01T00:52:00Z,15.43,mem,m,A,east
        ,,1,2022-12-31T05:41:24Z,2023-01-31T05:41:24.001Z,2023-01-01T00:52:00Z,59.25,mem,m,B,east
        ,,2,2022-12-31T05:41:24Z,2023-01-31T05:41:24.001Z,2023-01-01T00:52:00Z,52.62,mem,m,C,east
        EOD;
        $response = new Response(200, [], $data);
        break;

      default:
      case str_contains($body['query'], '(bucket: "example-noresponse")'):
        $response = new Response(200, [], '');
        break;
    }

    return new FulfilledPromise($response);
  }

}
