<?php

namespace Drupal\Tests\influxdb\Kernel;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Action\ActionManager;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\eca\Token\TokenInterface;
use Drupal\KernelTests\KernelTestBase;
use InfluxDB2\Point;
use Symfony\Component\Serializer\Serializer;

/**
 * Kernel tests for the "influxdb_create_point"-action plugin.
 *
 * @group influxdb
 */
class CreatePointTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'eca',
    'influxdb',
    'influxdb_bucket',
    'influxdb_bucket_eca',
    'key',
    'serialization',
    'system',
    'user',
  ];

  /**
   * The action manager.
   *
   * @var \Drupal\Core\Action\ActionManager|null
   */
  protected ?ActionManager $actionManager;

  /**
   * The token services.
   *
   * @var \Drupal\eca\Token\TokenInterface|null
   */
  protected ?TokenInterface $tokenServices;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer|null
   */
  protected ?Serializer $serializer;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installConfig(static::$modules);

    $this->actionManager = \Drupal::service('plugin.manager.action');
    $this->tokenServices = \Drupal::service('eca.token_services');
    $this->serializer = \Drupal::service('serializer');
  }

  /**
   * Test CreatePoint.
   */
  public function testCreatePoint(): void {
    $name = $this->randomString();
    $tags = [
      $this->randomMachineName() => $this->randomString(),
      $this->randomMachineName() => $this->randomString(),
      $this->randomMachineName() => $this->randomString(),
    ];
    $now = new DrupalDateTime();
    $fields = [
      $this->randomMachineName() => rand(0, 100),
      $this->randomMachineName() => rand(0, 100),
      $this->randomMachineName() => rand(0, 100),
    ];
    $tokenName = $this->randomString();

    /** @var \Drupal\influxdb_bucket_eca\Plugin\Action\CreatePoint $action */
    $action = $this->actionManager->createInstance('influxdb_create_point', [
      'name' => $name,
      'tags' => Yaml::encode($tags),
      'fields' => Yaml::encode($fields),
      'datetime' => $now->format('Y-m-d\TH:i:s.u0p'),
      'datetime_format' => 'Y-m-d\TH:i:s.u0p',
      'precision' => 's',
      'eca_token_name' => $tokenName,
    ]);
    $this->assertTrue($action->access(NULL));
    $action->execute();

    $data = $this->tokenServices->getTokenData($tokenName);
    $data = (array) $this->serializer->normalize($data);

    $point = Point::fromArray($data);
    $this->assertInstanceOf(Point::class, $point);

    ksort($data['tags']);
    $tagData = array_reduce(array_keys($data['tags']), function (array $carry, $key) use ($tags) {
      $carry[] = sprintf('%s=%s', addcslashes($key, ' =\\,'), addcslashes($tags[$key], ' =\\,'));
      return $carry;
    }, []);

    ksort($data['fields']);
    $fieldData = array_reduce(array_keys($data['fields']), function (array $carry, $key) use ($fields) {
      $carry[] = sprintf('%s=%si', addcslashes($key, ' =\\,'), $fields[$key]);
      return $carry;
    }, []);

    $this->assertEquals(sprintf('%s,%s %s %s', addcslashes($name, ' =\\,'), implode(',', $tagData), implode(',', $fieldData), $now->getTimestamp()), $point->toLineProtocol());
  }

}
