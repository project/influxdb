<?php

namespace Drupal\Tests\influxdb\Kernel;

use Drupal\Core\Action\ActionManager;
use Drupal\eca\Token\TokenInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Kernels tests for the "influxdb_run_flux_query"-action plugin.
 *
 * @group influxdb
 */
class RunFluxQueryTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'eca',
    'influxdb',
    'influxdb_bucket',
    'influxdb_bucket_eca',
    'influxdb_test',
    'key',
    'serialization',
    'system',
    'user',
  ];

  /**
   * The action manager.
   *
   * @var \Drupal\Core\Action\ActionManager|null
   */
  protected ?ActionManager $actionManager;

  /**
   * The token services.
   *
   * @var \Drupal\eca\Token\TokenInterface|null
   */
  protected ?TokenInterface $tokenServices;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installConfig(static::$modules);

    $this->actionManager = \Drupal::service('plugin.manager.action');
    $this->tokenServices = \Drupal::service('eca.token_services');
  }

  /**
   * Test no response.
   */
  public function testNoResponse(): void {
    $tokenName = $this->randomString();
    $query = <<<EOD
    from(bucket: "example-noresponse")
        |> range(start: -5m)
        |> filter(fn: (r) => r._measurement == "example-measurement")
    EOD;

    /** @var \Drupal\influxdb_bucket_eca\Plugin\Action\RunFluxQuery $action */
    $action = $this->actionManager->createInstance('influxdb_run_flux_query', [
      'query' => $query,
      'eca_token_name' => $tokenName,
    ]);
    $action->execute();

    /** @var \Drupal\eca\Plugin\DataType\DataTransferObject $data */
    $data = $this->tokenServices->getTokenData($tokenName);
    $this->assertIsArray($data->getValue());
    $this->assertEmpty($data->getValue());
  }

  /**
   * Test valid response.
   */
  public function testValidResponse(): void {
    $tokenName = $this->randomString();
    $query = <<<EOD
    from(bucket: "example-validresponse")
        |> range(start: -5m)
        |> filter(fn: (r) => r._measurement == "example-measurement")
    EOD;

    /** @var \Drupal\influxdb_bucket_eca\Plugin\Action\RunFluxQuery $action */
    $action = $this->actionManager->createInstance('influxdb_run_flux_query', [
      'query' => $query,
      'eca_token_name' => $tokenName,
    ]);
    $action->execute();

    /** @var \Drupal\eca\Plugin\DataType\DataTransferObject $data */
    $data = $this->tokenServices->getTokenData($tokenName);
    $values = $data->getValue()['values'];
    $this->assertIsArray($values);
    $this->assertCount(3, $values);

    $this->assertEquals(15.43, $values[0]['values']['_value']);
    $this->assertEquals('B', $values[1]['values']['host']);
    $this->assertEquals('east', $values[2]['values']['region']);
  }

}
